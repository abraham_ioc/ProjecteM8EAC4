/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.projectem8eac4;

import java.util.Scanner;

/**
 *
 * @author abraham
 */
public class ProjecteM8EAC4 {

    public static void main(String[] args) {
        try (Scanner scanner = new Scanner(System.in)) {
            System.out.println("Bienvenido al conversor de temperatura");
            System.out.println("Por favor, elige la conversión que deseas realizar:");
            System.out.println("1. Celsius a Fahrenheit");
            System.out.println("2. Fahrenheit a Celsius");
            System.out.println("3. Celsius a Kelvin");
            System.out.println("4. Kelvin a Celsius");
            System.out.print("Ingresa tu opción: ");
            
            int opcion = scanner.nextInt();
            
            switch (opcion) {
                case 1:
                    {
                        // Conversión de Celsius a Fahrenheit
                        System.out.print("Por favor, introduce la temperatura en grados Celsius: ");
                        double celsius = scanner.nextDouble();
                        double fahrenheit = (celsius * 9/5) + 32;
                        System.out.println(celsius + " grados Celsius equivalen a " + fahrenheit + " grados Fahrenheit.");
                        break;
                    }
                case 2:
                    {
                        // Conversión de Fahrenheit a Celsius
                        System.out.print("Por favor, introduce la temperatura en grados Fahrenheit: ");
                        double fahrenheit = scanner.nextDouble();
                        double celsius = (fahrenheit - 32) * 5/9;
                        System.out.println(fahrenheit + " grados Fahrenheit equivalen a " + celsius + " grados Celsius.");
                        break;
                    }
                case 3:
                    {
                        // Conversión de Celsius a Kelvin
                        System.out.print("Por favor, introduce la temperatura en grados Celsius: ");
                        double celsius = scanner.nextDouble();
                        double kelvin = celsius + 273.15;
                        System.out.println(celsius + " grados Celsius equivalen a " + kelvin + " Kelvin.");
                        break;
                    }
                case 4:
                    {
                        // Conversión de Kelvin a Celsius
                        System.out.print("Por favor, introduce la temperatura en Kelvin: ");
                        double kelvin = scanner.nextDouble();
                        double celsius = kelvin - 273.15;
                        System.out.println(kelvin + " Kelvin equivalen a " + celsius + " grados Celsius.");
                        break;
                    }
                default:
                    System.out.println("Opción no válida. Por favor, elige una opción del 1 al 4.");
                    break;
            }
        }
    }
}
